import csv_citanije as citanije
from datetime import datetime
import networkx as nx
import time
import pickle
import alive_progress
import konstante

def kreiraj_graf(): # funkcija za generisanje grafa
    svi_korisnici = citanije.ucitaj_korisnike_lista(konstante.korisnici_filepath)
    ##
    svi_korisnici_dict = citanije.ucitaj_korisnike(konstante.korisnici_filepath)  # ucitaj sve korisnike iz fajla
    sve_objave = citanije.ucitaj_objave(konstante.objave_filepath)
    sve_reakcije = citanije.ucitaj_reakcije(konstante.reakcije_filepath)
    sve_podele = citanije.ucitaj_podele(konstante.deljenja_filepath)
    svi_komentari = citanije.ucitaj_komentare(konstante.komentari_filepath)
    ##
    graf = nx.DiGraph()
    '''
    for korisnik in svi_korisnici:
    graf.add_node(korisnik)
    '''
    with alive_progress.alive_bar(len(svi_korisnici)) as bar:
        for korisnik_1 in svi_korisnici:
            for korisnik_2 in svi_korisnici:
                if korisnik_1 != korisnik_2:
                    tezina = izracunaj_afinitet(korisnik_1, korisnik_2, svi_korisnici_dict, sve_objave, sve_reakcije, sve_podele, svi_komentari)
                    if tezina != 0:
                        graf.add_edge(korisnik_1, korisnik_2, weight = tezina)
            bar()
    return graf

def izracunaj_afinitet(korisnik_self, korisnik_other, svi_korisnici_dict, sve_objave, sve_reakcije, sve_podele, svi_komentari): #funckija izracunava afinitet medju dva korisnika
    afinitet = 0 #pretpostavimo da su korisnici potpuno otudjeni
    danas = datetime.now()

    #prijateljstvo
    if korisnik_other in svi_korisnici_dict[korisnik_self]['prijatelji']:
        afinitet += 100 #algoritam strogo preferira objave korisnikovih prijatelja

    #istorija reakcija
    #pretrazi reakcije korisnika self i nadji one koje je objavio korisnik other
    if korisnik_self in sve_reakcije:
        for reakcija in sve_reakcije[korisnik_self]:
            if sve_objave[reakcija['id_objave']]['autor'] == korisnik_other:
                #interakcije starije od Biblije se ne smatraju preterano doprinosivim za trenutni afinitet dva korisnika
                datum_objave = datetime.strptime(reakcija['datum_reakcije'], '%Y-%m-%d %H:%M:%S')
                vremena_proslo = danas - datum_objave
                afinitet += 2 * vremenski_raspad_afinitet(vremena_proslo.days)

    #istorija deljenija
    if korisnik_self in sve_podele:
        for podela in sve_podele[korisnik_self]:
            if sve_objave[podela['id_objave']]['autor'] == korisnik_other:
                datum_objave = datetime.strptime(podela['datum_podele'], '%Y-%m-%d %H:%M:%S')
                vremena_proslo = danas - datum_objave
                datum_objave = datetime.strptime(reakcija['datum_reakcije'], '%Y-%m-%d %H:%M:%S')
                vremena_proslo = danas - datum_objave
                afinitet += 4 * vremenski_raspad_afinitet(vremena_proslo.days)

    #istorija komentarisanja
    if korisnik_self in svi_komentari:
        for komentar in svi_komentari[korisnik_self]:
            if sve_objave[komentar['id_objave']]['autor'] == korisnik_other:
                datum_objave = datetime.strptime(komentar['datum'], '%Y-%m-%d %H:%M:%S')
                vremena_proslo = danas - datum_objave
                datum_objave = datetime.strptime(reakcija['datum_reakcije'], '%Y-%m-%d %H:%M:%S')
                vremena_proslo = danas - datum_objave
                afinitet += 8 * vremenski_raspad_afinitet(vremena_proslo.days)
    return afinitet

def serijalizuj_graf(path):
    #serijalizuje graf preko krastavcica. vraca vreme izvrsavanja
    start_time = time.time()
    graf = kreiraj_graf()
    with open(path, "wb") as file:
        pickle.dump(graf, file)
    return round(time.time() - start_time)

def ucitaj_graf(path):
    #start_time = time.time()
    with open(path, "rb") as file:
        graf = pickle.load(file)
    #print("--- %s seconds ---" % (time.time() - start_time))
    return graf

def prijava(korisnicko_ime):
    svi_korisnici_lista = citanije.ucitaj_korisnike_lista(konstante.korisnici_filepath)
    return (korisnicko_ime in svi_korisnici_lista)

def vremenski_raspad_afinitet(broj_dana):
    koeficijent_raspada = 0.01
    if broj_dana == 0:
        return 70
    if broj_dana > 45:
        return koeficijent_raspada
    raspad = 1 / (koeficijent_raspada * broj_dana) - 2
    return raspad

if __name__ == '__main__':
    serijalizuj_graf(konstante.graf_filepath)