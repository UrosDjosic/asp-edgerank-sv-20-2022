import csv_citanije as citanije
import konstante
from edgerank import alt_edgerank

def analiziraj_unos(korisnicka_pretraga: str, korisnik_self):
    '''
    korisnickov unos moze uzeti tri oblika
    1) jednostavna pretraga. korisnik unosi jednu ili vise reci, pretrazuju se rezultati po svakoj, top 10 se izbacuje
    2) autocomplete pretraga. korisnik unosi pretragu pracenu sa *, nakon cega se pretrazuju objave koje sadrze reci koje pocinju sa time
    3) pretraga fraza. korisnik unosi pretragu pod navodnicima, izbacuje mu se rezultat svih nadjenih objava koje sadrze tacno tu frazu
    '''
    if korisnicka_pretraga.endswith('*'):
        return autocomplete(korisnicka_pretraga[:-1].lower(), korisnik_self)
    elif korisnicka_pretraga.startswith('"') or korisnicka_pretraga.startswith("'"):
        if korisnicka_pretraga.endswith('"') or korisnicka_pretraga.endswith("'"):
            return pretraga_fraza(korisnicka_pretraga[1:-1], korisnik_self)
    else:
        return jednostavna_pretraga(korisnicka_pretraga.lower().split(' '), korisnik_self)

def pretraga_fraza(uneta_fraza, korisnik_self):
    '''
    pretrazi objave za tacno trazenu frazu koju je korisnik uneo, zatim ih rangiraj
    '''
    nadjene_objave = []
    sve_objave = citanije.ucitaj_objave(konstante.objave_filepath)
    for _,objava in sve_objave.items():
        if objava['naziv'].__contains__(uneta_fraza):
            nadjene_objave.append(objava)
    return alt_edgerank(nadjene_objave, korisnik_self, 0)

def jednostavna_pretraga(lista_reci, korisnik_self):
    nadjene_objave = []
    sve_objave = citanije.ucitaj_objave(konstante.objave_filepath)
    for _, objava in sve_objave.items():
        broj_reci = 0
        for rec in lista_reci:
            broj_reci += objava['naziv'].lower().count(rec)
        if broj_reci != 0:
            nadjene_objave.append((objava, broj_reci))
    return alt_edgerank(nadjene_objave, korisnik_self, 1)

def autocomplete(unos, korisnik_self):
    nadjene_objave = []
    sve_objave = citanije.ucitaj_objave(konstante.objave_filepath)
    for _, objava in sve_objave.items():
        splitted = objava['naziv'].lower().split(' ')
        for splitter in splitted:
            if splitter.startswith(unos):
                nadjene_objave.append(objava)
                break
    return alt_edgerank(nadjene_objave, korisnik_self, 0)