import csv_citanije
from graf_korisnika import ucitaj_graf
from datetime import datetime
import konstante

def evaluacija_popularnosti_objave(objava: dict):
    # funkcija uzima pojedinacnu objavu i izracunava njenu all time popularnost. ne uzima vreme u obzir
    broj_reakcija = eval(objava['num_reactions'])
    broj_komentara = eval(objava['num_comments'])
    broj_podela = eval(objava['num_shares'])
    '''
    nisu svi kriterijumi popularnosti isti
    na primer, korisniku treba najmanje vremena da odreaguje na objavu. ako je poslao nekom, to je veci bonus. ako je izdvojio vremena da komentarise, to je i najbolje
    '''
    popularnost = round((broj_reakcija + 1.25 * broj_podela + 1.5 * broj_komentara))
    return popularnost

def vremenski_raspad(objava: dict):
    # funkcija uzima pojedinacnu objavu i izracunava koliko ce se puta umanjiti sansa za videti objavu po njenoj starost
    koeficijent_raspada = 0.01
    datum_objave = datetime.strptime(objava['datum'], '%Y-%m-%d %H:%M:%S')
    danasnjica = datetime.now()
    proteklo_dana = (danasnjica - datum_objave).days
    if proteklo_dana == 0:
        return 70
    if proteklo_dana > 45:
        return koeficijent_raspada
    raspad = 1 / (koeficijent_raspada * proteklo_dana) - 2
    return raspad


def ukupna_snaga_objave(objava, korisnik_self, graf):
    # afinitet * popularnost * vremenski raspad
    popularnost = evaluacija_popularnosti_objave(objava)
    vreme = vremenski_raspad(objava)
    try:
        #ako korisnici nemaju prethodne interakcije, samo kontaj da je jedan
        afinitet = graf[korisnik_self][objava['autor']]['weight']
        return round(popularnost * vreme * afinitet)
    except:
        return round(popularnost * vreme)

def edgerank(korisnik_self):
    #za svaku objavu uzima njen sadrzaj i stavlja je tuple cija je druga vrednost izracunata snaga objave
    #nakon toga, sve ide u listu koja se po tim vrednostima i rangira, i top 10 objava se vraca pozivniku
    graf = ucitaj_graf(konstante.graf_filepath)
    sve_objave = csv_citanije.ucitaj_objave(konstante.objave_filepath)
    lista_objava = []
    for _, objava in sve_objave.items():
        sadrzaj = f"{objava['naziv']} ({objava['tip']}):\n{objava['link']}"
        lista_objava.append((sadrzaj, ukupna_snaga_objave(objava, korisnik_self, graf)))
    #sortiraj to
    lista_objava = sorted(lista_objava, key=lambda x: x[1], reverse=True)
    return lista_objava[:10]

def alt_edgerank(rezultati_pretrage, korisnik_self, case):
    graf = ucitaj_graf(konstante.graf_filepath)
    lista_objava = []
    if case == 0:
        for objava in rezultati_pretrage:
            sadrzaj = f"{objava['naziv']} ({objava['tip']}):\n{objava['link']}"
            lista_objava.append((sadrzaj, ukupna_snaga_objave(objava, korisnik_self, graf)))
        # sortiraj to
        lista_objava = sorted(lista_objava, key=lambda x: x[1], reverse=True)
        if len(lista_objava) >= 10:
            return lista_objava[:10]
        else:
            return lista_objava
    else:
        for objava in rezultati_pretrage:
            sadrzaj = f"{objava[0]['naziv']} ({objava[0]['tip']}):\n{objava[0]['link']}"
            lista_objava.append((sadrzaj, objava[1] * ukupna_snaga_objave(objava[0], korisnik_self, graf)))
        # sortiraj to
        lista_objava = sorted(lista_objava, key=lambda x: x[1], reverse=True)
        if len(lista_objava) >= 10:
            return lista_objava[:10]
        else:
            return lista_objava