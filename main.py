from edgerank import edgerank
from graf_korisnika import prijava
from pretraga import analiziraj_unos
#simulator same aplikacije koja primenjuje edge rank algoritam

if __name__ == '__main__':
    #pocetni meni
    print('\n=== DOBRODOSLI ===\n')
    #prijava
    ulogovan = False
    while not ulogovan:
        korisnicko_ime = input('Korisnicko ime: ')
        ulogovan = prijava(korisnicko_ime)
        if not ulogovan:
            print('\nTakav korisnik ne postoji!\n')
    #aplikacija!
    while True:
        print('\n1) Pregledajte Objave\n2) Pretraga\n3) Izlaz')
        unos = input('Odaberite jednu od opcija: ')
        if unos == '1' or unos == '1)':
            print('\nUcitavam objave...\n')
            top_10 = edgerank(korisnicko_ime)
            for objava in top_10:
                print(f"\n{objava[0]}\n")
        elif unos == '2' or unos == '2)':
            korisnicka_pretraga = input('Unesite svoju pretragu: ')
            top_10 = analiziraj_unos(korisnicka_pretraga, korisnicko_ime)
            for objava in top_10:
                print(f"\n{objava[0]}\n")
        elif unos == '3' or unos == '3)':
            print("\n=== S'BOGOM ===\n")
            exit()