class TrieNode():
    def __init__(self) -> None:
        #ne cuva data, samo bool i dict dece
        self._is_final = False
        self._children = {}

class Trie():
    def __init__(self) -> None:
        #inicijalizuje se praznim poljem
        self._root = TrieNode()
    
    def insert(self, word) -> None:
        #kreni od pocetka
        current_node = self._root
        #za svaki char - ako se ne nalazi u deci dodaj ga, ako da samo predji na sledeci node i gledaj njegovu decu
        for char in word:
            if char not in current_node._children:
                current_node._children[char] = TrieNode()
            current_node = current_node._children[char]
        #finalizuj
        current_node._is_final = True
    
    def search(self, word) -> bool:
        #pretrazi stablo. ako rec postoji i zadnje slovo je finalizacija reci, onda je rec u stablu
        current_node = self._root
        for char in word:
            if char not in current_node._children:
                return False
            current_node = current_node._children[char]
        return current_node._is_final
    
    def starts_with(self, prefix) -> bool:
        #prefiks pretraga (metoda izbacena iz implementacije projekta. razlog - suvisna)
        current_node = self._root
        for char in prefix:
            if char not in current_node._children:
                return False
            current_node = current_node._children[char]
        return True
    
if __name__ == '__main__':
    traj = Trie()
    traj.insert('gari')
    traj.insert('lignjoslav')
    traj.insert('sundjerbob')
    traj.insert('patrik')
    #
    print(traj.search('gari'))
    print(traj.search('kebakraba'))
    #
    print(traj.starts_with('lign'))
    print(traj.starts_with('jor'))