def ucitaj_korisnike(path):
    #funkcija ucitava korisnike iz fajla
    svi_korisnici = {}
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines[1:]: #zanemari prvu liniju, ona je samo header csv fajla iz nekog razloga
            data = line.split(',')
            osoba = {}
            osoba['ime'] = data[0]
            osoba['broj_prijatelja'] = int(data[1])
            prijatelji = [] #jako goofy csv fajl, mora da je postojalo barem 5 boljih nacina za realizaciju al ajde
            for i in range(2, int(data[1]) + 2):
                prijatelji.append(data[i])
            osoba['prijatelji'] = prijatelji
            if osoba['ime'] not in svi_korisnici:
                svi_korisnici[data[0]] = osoba
    return svi_korisnici

def ucitaj_objave(path):
    #vraca objace kroz dict
    sve_objave = {}
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        comment = ""
        paired_ellipses = True

        for index in range(1, len(lines)):

            line = lines[index]

            if line == "\n":
                comment += line
                continue

            line = line.strip()

            previous_index = -1

            while True:
                index = line.index("\"", previous_index + 1) if "\"" in line[previous_index + 1:] else -1
                if index == -1:
                    break
                paired_ellipses = not paired_ellipses
                previous_index = index

            comment += line
            if not paired_ellipses:
                continue

            data = comment.split(",")
            n = len(data)

            if n < 16:
                raise Exception("Objava nije validna!")
            elif n > 16:
                comment_text = "".join(data[1:n - 14])
            else:
                comment_text = data[1]

            #iskreno ne znam sta se desava u kodu gore
            objava = {
                'id': data[0],
                'naziv': comment_text,
                'tip': data[n - 14],
                'link': data[n - 13],
                'datum': data[n - 12],
                'autor': data[n - 11],
                'num_reactions': data[n - 10],
                'num_comments': data[n - 9],
                'num_shares': data[n - 8],
                'num_likes': data[n - 7],
                'num_loves': data[n - 6],
                'num_wows': data[n - 5],
                'num_hahas': data[n - 4],
                'num_sads': data[n - 3],
                'num_angrys': data[n - 2],
                'num_special': data[n - 1]
            }
            sve_objave[data[0]] = objava
            comment = ""
            paired_ellipses = True

    return sve_objave

def ucitaj_reakcije(path):
    #sve reakcije se citaju na sledeci nacin
    '''
    hashmap
        ime korisnika
            svi dictovi koji njegovo ime sadrze
    '''
    sve_reakcije = {}
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines[1:]:  # zanemari prvu liniju, ona je samo header csv fajla iz nekog razloga
            data = line.split(',')
            if data[2] not in sve_reakcije: #korisnik
                sve_reakcije[data[2]] = [] #lista koja ce sadrzati sve korisnikove reakcije
                reakcija = {
                    'id_objave': data[0],
                    'tip_reakcije': data[1],
                    'osoba_reagovala': data[2],
                    'datum_reakcije': data[3].rstrip('\n')
                }
                sve_reakcije[data[2]].append(reakcija)
            else: #ako korisnik vec postoji, uradi istu stvar samo bez resetovanja liste na []
                reakcija = {
                    'id_objave': data[0],
                    'tip_reakcije': data[1],
                    'osoba_reagovala': data[2],
                    'datum_reakcije': data[3].rstrip('\n')
                }
                sve_reakcije[data[2]].append(reakcija)
    return sve_reakcije

def ucitaj_podele(path):
    # svi shareovi se citaju na sledeci nacin
    '''
    hashmap
        ime korisnika
            svi dictovi koji njegovo ime sadrze
    '''
    sve_podele = {}
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines[1:]:  # zanemari prvu liniju, ona je samo header csv fajla iz nekog razloga
            data = line.split(',')
            if data[1] not in sve_podele:  # korisnik
                sve_podele[data[1]] = []  # lista koja ce sadrzati sve korisnikove reakcije
                podela = {
                    'id_objave': data[0],
                    'osoba_podelila': data[1],
                    'datum_podele': data[2].rstrip('\n')
                }
                sve_podele[data[1]].append(podela)
            else:  # ako korisnik vec postoji, uradi istu stvar samo bez resetovanja liste na []
                podela = {
                    'id_objave': data[0],
                    'osoba_podelila': data[1],
                    'datum_podele': data[2].rstrip('\n')
                }
                sve_podele[data[1]].append(podela)
    return sve_podele

def ucitaj_komentare(path):
    svi_komentari = {}
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        comment = ""
        found_open_ellipsis = False
        found_close_ellipsis = False

        for index in range(1, len(lines)):

            line = lines[index]

            if line == "\n":
                comment += line

            if line[-1] == "\n":
                line = line[:-1]

            first_index = line.index("\"") if "\"" in line else -1
            if first_index > -1:
                found_open_ellipsis = True

            next_index = line.index("\"", first_index + 1) if "\"" in line[first_index + 1:] else -1
            if next_index > -1:
                found_close_ellipsis = True

            if found_open_ellipsis and not found_close_ellipsis:
                comment += line
                continue
            else:
                comment = line

            data = comment.split(",")
            n = len(data)

            if n < 14:
                raise Exception("Comment does not contain necessary data.")
            elif n > 14:
                comment_text = "".join(data[3:n - 10])
            else:
                comment_text = data[3]

            if data[4] not in svi_komentari: #ako nismo vec ubiljezili doga nam komentatora (ista kolekzija kao reagzije i podele)
                svi_komentari[data[4]] = []
                komentar = {
                    'id_komentara': data[0],
                    'id_objave': data[1],
                    'roditelj': data[2],
                    'sadrzaj': comment_text,
                    'komentator': data[n - 10],
                    'datum': data[n - 9],
                    'num_reactions': data[n - 8],
                    'num_likes': data[n - 7],
                    'num_loves': data[n - 6],
                    'num_wows': data[n - 5],
                    'num_hahas': data[n - 4],
                    'num_sads': data[n - 3],
                    'num_angrys': data[n - 2],
                    'num_special': data[n - 1]
                }
                svi_komentari[data[4]].append(komentar)
            else:
                komentar = {
                    'id_komentara': data[0],
                    'id_objave': data[1],
                    'roditelj': data[2],
                    'sadrzaj': comment_text,
                    'komentator': data[n - 10],
                    'datum': data[n - 9],
                    'num_reactions': data[n - 8],
                    'num_likes': data[n - 7],
                    'num_loves': data[n - 6],
                    'num_wows': data[n - 5],
                    'num_hahas': data[n - 4],
                    'num_sads': data[n - 3],
                    'num_angrys': data[n - 2],
                    'num_special': data[n - 1]
                }
                svi_komentari[data[4]].append(komentar)
            found_open_ellipsis = found_close_ellipsis = False
            comment = ""
    return svi_komentari

def ucitaj_korisnike_lista(path):
    svi_korisnici = []
    with open(path, encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines[1:]:  # zanemari prvu liniju, ona je samo header csv fajla iz nekog razloga
            data = line.split(',')
            svi_korisnici.append(data[0])
    return svi_korisnici