'''
filepath konstante
'''

komentari_filepath = 'dataset/original_comments.csv'
reakcije_filepath = 'dataset/original_reactions.csv'
deljenja_filepath = 'dataset/original_shares.csv'
objave_filepath = 'dataset/original_statuses.csv'

korisnici_filepath = 'dataset/friends.csv'

graf_filepath = 'serijalizovan_graf.pkl'